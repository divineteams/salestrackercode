

export class User {
    UserID: number = 0;
    RoleID: number = 0;
    EntityID: number = 0;
    UserName: string = "";
    Password: string = "";
    FirstName: string = "";
    LastName: string = "";
    IsActive: boolean = true;
    IsOnline: boolean = false;
    LastLoginDate;
    CreatedBy: number;
    CreatedDate;
    ModifiedBy;
    ModifiedDate;

    constructor() {
        this.UserID = 0;
        this.RoleID;
        this.EntityID;
        this.UserName = "";
        this.Password = "";
        this.FirstName = "";
        this.LastName = "";
        this.IsActive = true;
        this.IsOnline = false;
        this.LastLoginDate = "";
        this.CreatedBy;
        this.CreatedDate = "";
        this.ModifiedBy = "";
        this.ModifiedDate = "";
    }
  
}
