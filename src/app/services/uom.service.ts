import { Injectable } from '@angular/core';
import { URLSearchParams, RequestOptions,Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AuthHttp } from 'angular2-jwt';

import { Global } from '../shared/Global';
import { UOM } from '../Domain/uom';

@Injectable()

export class UOMService {

  private global:Global = new Global();

  constructor(private authhttp: AuthHttp) {  } 

  GetUOM(UOMID) : Observable<UOM[]>{ 
    
    let params: URLSearchParams = new URLSearchParams();
    params.set('id', UOMID);

    let requestOptions = new RequestOptions();
    requestOptions.search = params;

     return this.authhttp.get(this.global.baseUrl + "UOM/GetList", requestOptions)
     .map(res => res.json(),
      err => this.handleError(err));       
  }

//   AddUser(user: User){
//        return this.authhttp.post(this.global.baseUrl + 'Users/Register', JSON.stringify(user))
//      .map(a =>  { return a.status == 200 },err => this.handleError(err));
//  }
 
//  GetRole() : Observable<Role[]>{
//      return this.authhttp.get(this.global.baseUrl + "users/GetRoleList")
//      .map(res => res.json(),
//       err => this.handleError(err));   
//  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}