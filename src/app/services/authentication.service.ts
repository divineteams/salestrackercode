import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams, RequestOptions, RequestMethod, Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { tokenNotExpired, AuthHttp} from 'angular2-jwt';

import { Global } from '../shared/Global';
import { User } from "../domain/user";

@Injectable()

export class AuthenticationService {

  private global: Global = new Global();

  constructor(private authHttp : AuthHttp , private _router: Router) {
  }

  Login(user: User) {
    return this.authHttp.post(this.global.baseUrl + 'Users/Login', JSON.stringify(user))
      .map(res => res.text(),
      err => this.handleError(err));
  }

  Logout(usename: string) {
   
    let params: URLSearchParams = new URLSearchParams();
    params.set('UserName', usename);

    let requestOptions = new RequestOptions();
    requestOptions.search = params;

    return this.authHttp.get(this.global.baseUrl + 'Users/Logout', requestOptions)
      .map(a => { return a.status == 200 },
      err => this.handleError(err));
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
