import { NgModule } from '@angular/core';

import {
    AuthenticationService,
    UserService,
    UOMService,
    ValidationService
} from '../services';

@NgModule({
    providers: [
        AuthenticationService,
        UserService,
        UOMService,
        ValidationService
    ]

})
export class ServicesModule { }
