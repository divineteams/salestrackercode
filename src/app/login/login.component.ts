import { NgModule, Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthenticationService,ValidationService } from '../services';
import { User } from '../domain/user'
import { Router } from '@angular/router';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [AuthenticationService]
})

export class LoginComponent implements OnInit {
    public user = new User();

    constructor( private _service: AuthenticationService, private router: Router) {
        this.user.UserName = "admin28";
        this.user.Password = "maniyar37";
    }

    loginform: FormGroup;
    UserName: FormControl;
    Password: FormControl;

    ngOnInit() {
        this.UserName = new FormControl('', Validators.required);
        this.Password = new FormControl('', [Validators.required, ValidationService.passwordValidator]);
        this.loginform = new FormGroup({
            UserName: this.UserName,
            Password: this.Password
        });
    }

    onlogin() {
        this._service.Login(this.user)
            .subscribe(
            data => {
                if (data != "") {
                    localStorage.setItem('id_token', data),
                        this.router.navigate(['../dashboard']);
                }
                else {
                    alert("Invalid username or Password.");
                }
            });
    }
}