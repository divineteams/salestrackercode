import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule  }   from '@angular/forms'; 

import { UserRoutingModule } from './user-routing.module';
import { UserListComponent } from '../user/List/userlist.component';
import { UserAddUpdateComponent } from '../user/addupdate/userAddUpdate.component';

// import { SharedPipesModule } from '../../shared/pipes/shared-pipes.module';
import { MyUppercaseOnePipe } from '../../shared/pipes/myuppercaseone.pipe'
 import { dateFormatPipe } from '../../shared/pipes/date.pipe'
 
@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule ,
    FormsModule,
    TranslateModule
    // SharedPipesModule
  ],
  declarations: [
    UserListComponent,
    UserAddUpdateComponent,
    MyUppercaseOnePipe,
    dateFormatPipe
    ]
})
export class UserModule { }

