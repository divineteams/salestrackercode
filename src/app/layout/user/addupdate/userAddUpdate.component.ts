import { Component, OnInit } from '@angular/core';
import { FormsModule, AbstractControl, FormBuilder, Validators}   from '@angular/forms'; 

import { Global } from "app/shared/Global";
import { dateFormatPipe } from "app/shared/pipes/date.pipe";
import { User } from "app/domain/user";
import { Role } from "app/domain/role";
import { UserService } from "app/services/user.service";

@Component({
  selector: 'app-userAddUpdate',
  templateUrl: './userAddUpdate.component.html'
})
export class UserAddUpdateComponent implements OnInit{
public user = new User();
public role = new Role();
private global:Global = new Global();
private dateformatpipe:dateFormatPipe = new dateFormatPipe();

 NewRoles: Role[];
 
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.GetRole().subscribe(res=>this.NewRoles=res);
}

onChange(newVal) {
    this.role.RoleID = newVal;
} 

  onregister(){
      this.user.RoleID = this.role.RoleID
      this.user.EntityID = 1
      this.user.IsActive = true
      this.user.IsOnline = false
      this.user.CreatedBy = Number(this.global.GetUserID()) 
      this.user.CreatedDate = this.dateformatpipe.transform(Date.now().toString())
      alert( this.user.CreatedDate)

     // this.userService.AddUser(this.user).subscribe();

      if (this.userService.AddUser(this.user).subscribe(res => {return res == true}))
            {
               alert("User register Successfully");
            }
  }
}
