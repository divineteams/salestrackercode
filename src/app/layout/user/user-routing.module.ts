import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from '../user/List/userlist.component';
import { UserAddUpdateComponent } from '../user/addupdate/userAddUpdate.component';

const routes: Routes = [
  { path: '', redirectTo: "list" },
  {
    path: '',
    children: [ 
      { path: 'list', component: UserListComponent },
      { path: 'add', component: UserAddUpdateComponent },
      { path: 'edit', component: UserAddUpdateComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
