import { Component, OnInit } from '@angular/core';
import { UOMService } from '../../../../services/uom.service';
import { UOM } from '../../../../Domain/uom';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {
  errorMessage: string;
  uoms: UOM[];
  constructor(private uomService: UOMService) { }

  ngOnInit() {
     this.uomService.GetUOM(0).subscribe(resp => this.uoms = resp);
  }
}
