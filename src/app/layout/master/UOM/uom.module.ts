import { NgModule } from '@angular/core';
import { CommonModule , DatePipe} from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { UOMRoutingModule } from './uom-routing.module';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { SharedModule } from '../../../shared';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UOMRoutingModule,
    FormsModule,ReactiveFormsModule,
  ],
  declarations: [AddComponent, ListComponent,
    ]
})

export class UOMModule { }
