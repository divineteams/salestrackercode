import { NgModule,  Component, OnInit,ChangeDetectionStrategy } from '@angular/core';
import { ReactiveFormsModule, FormsModule,  FormGroup, FormControl, Validators} from '@angular/forms';

import { ValidationService } from '../../../../services/validation.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-add',
  templateUrl: './add.component.html'
})

export class AddComponent implements OnInit {

  uomform : FormGroup;
  Name : FormControl;
  Abbr : FormControl;

  constructor() { }

  ngOnInit() {
        this.Name = new FormControl('', Validators.required);
        this.Abbr = new FormControl('', Validators.required);
        this.uomform = new FormGroup({
            Name: this.Name,
            Abbr: this.Abbr
        });
  }
}
