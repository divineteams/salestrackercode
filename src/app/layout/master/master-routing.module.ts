import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: "uom" },
    { path: '',
        children: [
            { path: 'uom', loadChildren: './UOM/uom.module#UOMModule' }
        ]
    }
];  


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MasterRoutingModule { }
