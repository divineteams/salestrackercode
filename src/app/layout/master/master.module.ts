import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MasterRoutingModule } from './master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MasterRoutingModule,
    TranslateModule
  ]
})
export class MasterModule { }
