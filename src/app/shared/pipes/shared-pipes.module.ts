import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { dateFormatPipe } from '../pipes/date.pipe'
import { MyUppercaseOnePipe } from '../pipes/myuppercaseone.pipe'

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [dateFormatPipe,MyUppercaseOnePipe]
})
export class SharedPipesModule { }
