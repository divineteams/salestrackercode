import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'DateFormate',
})
export class dateFormatPipe implements PipeTransform {
    
    transform(value: string) {
       var datePipe = new DatePipe("en-US");
        value = datePipe.transform(value, 'yyyy-MM-dd HH:mm:ss.ms');
        return value;
    }
}   