import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { HeaderComponent, SidebarComponent, ControlMessagesComponent } from '../components';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule
    ],
    declarations: [HeaderComponent, SidebarComponent, ControlMessagesComponent ],
    exports: [HeaderComponent, SidebarComponent, ControlMessagesComponent]
})

export class SharedModule { }