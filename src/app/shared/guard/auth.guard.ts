import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Global } from '../Global';

@Injectable()
export class AuthGuard implements CanActivate {

private global:Global = new Global();

    constructor(private router: Router) {
         
     }
    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
        let roles = route.data["roles"] as Array<string>;

        if (roles == null || roles.indexOf(this.global.GetRole()) == -1) {
            this.router.navigate(['./not-found']);
            return false;
        }
        return true;
    }


}
