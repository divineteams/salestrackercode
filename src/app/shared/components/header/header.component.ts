import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { Global } from '../../Global';
import { UserService } from '../../../services/user.service';
import { AuthenticationService } from '../../../services'
import { User } from '../../../domain/user';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    private global:Global = new Global();
    public user : User = new User();

    constructor(private translate: TranslateService,  
     private Authservice: AuthenticationService,
     private userService : UserService,private router: Router) { }

    ngOnInit() {
         this.userService.GetUser(Number(this.global.GetUserID()))
            .subscribe(res => {this.user = res[0]});
     }

    onLoggedout() {
        var Username = this.global.GetUsername();
        if (Username != "")
         {
            if (this.Authservice.Logout(Username).subscribe(res => {return res == true}))
            {
                localStorage.removeItem('id_token');
                this.router.navigate(['login']);
            }
        }
    }
    
    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('push-right');
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }
}
