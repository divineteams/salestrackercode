import { JwtHelper, tokenNotExpired } from 'angular2-jwt';

export class Global {
    jwtHelper = new JwtHelper();
    token = "";
    Decodedtoken: any;
    baseUrl = 'http://localhost:50688/api/';

    constructor() {
        this.token = localStorage.getItem('id_token');
        this.Decodedtoken = this.token != null ? this.jwtHelper.decodeToken(this.token) : "";
    }

    GetRole(): string {

        if (tokenNotExpired("id_token") && this.Decodedtoken != null) 
            return this.Decodedtoken.role;
        else
            return "";
    }

    GetUsername(): string {
        if (tokenNotExpired("id_token") && this.Decodedtoken != null)
            return this.Decodedtoken.unique_name;
        else
            return "";
    }

    GetUserID(): string {
        if (tokenNotExpired("id_token") && this.Decodedtoken != null) 
            return this.Decodedtoken.nameid;
        else
            return "";
    }
}